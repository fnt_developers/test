const init = () => {

  const xhr = new XMLHttpRequest()
  const sidebar = document.querySelector('#js-load-sidebar-document')
  xhr.open('GET', '/pages/document/sidebar.html', true)
  xhr.onreadystatechange = () => {
    if(xhr.readyState === 4 && xhr.status === 200) {
      const restxt = xhr.responseText
      sidebar.innerHTML = restxt
    }
  }
  xhr.send()

}

window.addEventListener('DOMContentLoaded', () => {
  init()
})
